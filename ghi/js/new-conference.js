window.addEventListener("DOMContentLoaded", async() =>{

    // Populate Locations
    const locationUrl = 'http://localhost:8000/api/locations/'
    const response = await fetch(locationUrl);

    if (!response.ok){
        console.log("Bad Response, try again!")
    }else{
        const data = await response.json();
        const stateSelect = document.getElementById("location");
        for (let location of data.locations){
            const option = document.createElement("option");
            option.innerHTML = location.name;
            option.value = location.id;
            stateSelect.appendChild(option);
        }
    }

    // Add submit event
    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: 'post',
            body: json,
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok){
            formTag.reset();
            const newConference = await response.json();
        };
    });

});
