window.addEventListener("DOMContentLoaded", async () =>{
    //populate states
    const states_url = 'http://localhost:8000/api/states/'

    const response = await fetch(states_url);
    if (!response.ok){
        console.log("Bad Response, try again!")
    }else{
        const data = await response.json();
        const stateSelect = document.getElementById("state");
        for (let state of data.states){
            const option = document.createElement("option");
            option.innerHTML = state.name;
            option.value = state.abbreviation;
            stateSelect.appendChild(option);
        }
    }

    //add submit event
    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: 'post',
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok){
            formTag.reset();
            const newLocation = await response.json();
        }

    });

});
